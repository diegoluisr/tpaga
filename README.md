#Installation with Composer
```
composer require diegoluisr/tpaga
```

#Usage
...

#Usage Notes
When a new developer clones your codebase, they will have an additional one-time step to manually copy the .env.example file to .env and fill-in their own values (or get any sensitive values from a project co-worker).

#Security
If you discover a security vulnerability within this package, please send an email to Diego Restrepo at [diegoluisr@gmail.com](mailto:diegoluisr@gmail.com). All security vulnerabilities will be promptly addressed.

#License
TPaga PHP SDK is licensed under MIT License.