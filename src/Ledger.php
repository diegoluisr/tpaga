<?php
/**
 * Este doumento es para uso exclusivo de Yampi/Andali
 * file: app/Http/Controllers/AbuseController.php
 * 
 * PHP version 7
 * 
 * @category HttpController
 * @package  App\Http\Controllers
 * @author   Diego Luis Restrepo <drestrepo@andali.co>
 * @license  http://andali.co/license/commercial.txt PHP Commercial 1.0
 * @link     http://yampi.co/php-docs/package/App/Http/Controllers
 */

namespace TPaga;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

/**
 * Clase para gestionar los trabajos en el envio de notificaciones push.
 * 
 * @category HttpController
 * @package  App\Http\Controllers
 * @author   Diego Luis Restrepo <drestrepo@andali.co>
 * @license  http://andali.co/license/commercial.txt PHP Commercial 1.0
 * @link     http://ymapi.co/php-docs/package/App/Http/Controllers
 */
class Ledger
{

    /**
     * Static function to solve a transfer
     * 
     * @param string $account     Transaction target account
     * @param string $bucket      Transaction target bucket
     * @param string $description Transaction description
     * @param int    $amount      Transaction amount
     * @param string $tx_id       Transaction unique ID
     * 
     * @return ResponseInterface
     */
    public static function transfer(
        string $account,
        string $bucket,
        string $description,
        string $amount,
        string $tx_id
    ) {
        $client = new Client();
        $response = $client->request(
            'POST', $_ENV['TPAGA_LEDGER_PATH'] . '/my/transfer', [
                'json' => [
                    'destination_account' => $account,
                    'destination_account_bucket' => $bucket,
                    'description' => $description,
                    'amount' => $amount,
                    'unique_transfer_token' => $tx_id,
                ],
                'headers' => [
                    'X-Account-Api-Key' => $_ENV['TPAGA_LEDGER_APIKEY'],
                    'Content-Type' => 'application/json',
                ]
            ]
        );

        return $response;
    }

    /**
     * Static function to solve a transfer
     * 
     * @return ResponseInterface
     */
    public static function balance()
    {
        $client = new Client();
        $response = $client->request(
            'GET', $_ENV['TPAGA_LEDGER_PATH'] . '/my/balance', [
                'headers' => [
                    'X-Account-Api-Key' => $_ENV['TPAGA_LEDGER_APIKEY'],
                    'Content-Type' => 'application/json',
                ]
            ]
        );

        return $response;
    }
}