<?php
/**
 * Este doumento es para uso exclusivo de Yampi/Andali
 * file: app/Http/Controllers/AbuseController.php
 * 
 * PHP version 7
 * 
 * @category HttpController
 * @package  App\Http\Controllers
 * @author   Diego Luis Restrepo <drestrepo@andali.co>
 * @license  http://andali.co/license/commercial.txt PHP Commercial 1.0
 * @link     http://yampi.co/php-docs/package/App/Http/Controllers
 */

namespace TPaga;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use TPaga\Utils\Utils;

/**
 * Clase para gestionar los trabajos en el envio de notificaciones push.
 * 
 * @category HttpController
 * @package  App\Http\Controllers
 * @author   Diego Luis Restrepo <drestrepo@andali.co>
 * @license  http://andali.co/license/commercial.txt PHP Commercial 1.0
 * @link     http://ymapi.co/php-docs/package/App/Http/Controllers
 */
class Merchant
{

    /**
     * Static function to solve a transfer
     * 
     * @param int    $cost                 Cost
     * @param string $purchase_details_url Purchase detail URL
     * @param string $voucher_url          Voucher URL
     * @param string $idempotency_token    Idempotency token
     * @param string $order_id             Order ID
     * @param string $terminal_id          Terminal ID
     * @param string $purchase_description Purchase description
     * @param array  $items                Items array {$name, $value}
     * 
     * @return ResponseInterface
     */
    public static function paymentCreate(
        int $cost,
        string $purchase_details_url,
        string $voucher_url,
        string $idempotency_token,
        string $order_id,
        string $terminal_id,
        string $purchase_description,
        array $items
    ) {
        $client = new Client();
        $response = $client->request(
            'POST', $_ENV['TPAGA_MERCHANT_PATH'] . '/payment_requests/create', [
                'headers' => [
                    'Authorization' => 'Basic '
                        . Utils::getAuth(
                            $_ENV['TPAGA_MERCHANT_USER'],
                            $_ENV['TPAGA_MERCHANT_PASSWORD']
                        ),
                    'Cache-Control' => 'no-cache',
                    'Content-Type' => 'application/json',
                ],
                'json' => [
                    'cost' => $cost,
                    'purchase_details_url' => $purchase_details_url,
                    'voucher_url' => $voucher_url,
                    'idempotency_token' => $idempotency_token,
                    'order_id' => $order_id,
                    'terminal_id' => $terminal_id,
                    'purchase_description' => $purchase_description,
                    'purchase_items' => $items,
                    'user_ip_address' => Utils::getIpAddress(),
                    'expires_at' => Carbon::now('America/Bogota')
                        ->addHours(12)->format('c')
                ],
            ]
        );

        return $response;
    }

    /**
     * Static function to get payment status/info
     * 
     * @param string $payment_token_id Payment token ID
     * 
     * @return ResponseInterface
     */
    public static function paymentInfo(string $payment_token_id)
    {
        $client = new Client();
        $response = $client->request(
            'GET', $_ENV['TPAGA_MERCHANT_PATH']
                . '/payment_requests/' . $payment_token_id . '/info', [
                'headers' => [
                    'Authorization' => 'Basic '
                        . Utils::getAuth(
                            $_ENV['TPAGA_MERCHANT_USER'],
                            $_ENV['TPAGA_MERCHANT_PASSWORD']
                        ),
                    'Cache-Control' => 'no-cache',
                    'Content-Type' => 'application/json',
                ]
            ]
        );

        return $response;
    }

    /**
     * Static function to confirm delivery
     * 
     * @param string $payment_token_id Payment token ID
     * 
     * @return ResponseInterface
     */
    public static function deliveryConfirm(string $payment_token_id)
    {
        $client = new Client();
        $response = $client->request(
            'POST', $_ENV['TPAGA_MERCHANT_PATH']
                . '/payment_requests/confirm_delivery', [
                'headers' => [
                    'Authorization' => 'Basic '
                        . Utils::getAuth(
                            $_ENV['TPAGA_MERCHANT_USER'],
                            $_ENV['TPAGA_MERCHANT_PASSWORD']
                        ),
                    'Cache-Control' => 'no-cache',
                    'Content-Type' => 'application/json',
                ],
                'json' => [
                    'payment_request_token' => $payment_token_id
                ],
            ]
        );

        return $response;
    }

    /**
     * Static function to refound the payment
     * 
     * @param string $payment_token_id Payment token ID
     * 
     * @return ResponseInterface
     */
    public static function paymentRefound(string $payment_token_id)
    {
        $client = new Client();
        $response = $client->request(
            'POST', $_ENV['TPAGA_MERCHANT_PATH'] . '/payment_requests/refund', [
                'headers' => [
                    'Authorization' => 'Basic '
                        . Utils::getAuth(
                            $_ENV['TPAGA_MERCHANT_USER'],
                            $_ENV['TPAGA_MERCHANT_PASSWORD']
                        ),
                    'Cache-Control' => 'no-cache',
                    'Content-Type' => 'application/json',
                ],
                'json' => [
                    'payment_request_token' => $payment_token_id
                ],
            ]
        );

        return $response;
    }
}
