<?php
/**
 * Este doumento es para uso exclusivo de Yampi/Andali
 * file: app/Http/Controllers/AbuseController.php
 * 
 * PHP version 7
 * 
 * @category Utility
 * @package  TPaga\Utils
 * @author   Diego Luis Restrepo <diegoluisr@gmail.com>
 * @license  http://andali.co/license/commercial.txt PHP MIT 1.0
 * @link     http://yampi.co/php-docs/package/App/Http/Controllers
 */

namespace TPaga\Utils;

/**
 * Utility class to provide convenience functions.
 * 
 * @category HttpController
 * @package  App\Http\Controllers
 * @author   Diego Luis Restrepo <drestrepo@andali.co>
 * @license  http://andali.co/license/commercial.txt PHP Commercial 1.0
 * @link     http://ymapi.co/php-docs/package/App/Http/Controllers
 * // Credits to manuelbcd <https://stackoverflow.com/users/3518053/manuelbcd>
 * // https://stackoverflow.com/questions/3003145/how-to-get-the-client-ip-address-in-php
 */
class Utils
{

    /**
     * Convert an user and a password string to Base64
     * 
     * @param string $user     User string
     * @param string $password User password
     * 
     * @return string
     */
    public static function getAuth(string $user, string $password)
    {
        return base64_encode($user . ':' . $password);
    }

    /**
     * Get the request IP addres
     * 
     * @return string
     */
    public static function getIpAddress()
    {

        // Check for shared Internet/ISP IP
        if (!empty($_SERVER['HTTP_CLIENT_IP'])
            && self::validateIp($_SERVER['HTTP_CLIENT_IP'])
        ) {
            return $_SERVER['HTTP_CLIENT_IP'];
        }
    
        // Check for IP addresses passing through proxies
        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    
            // Check if multiple IP addresses exist in var
            if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',') !== false) {
                $iplist = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                foreach ($iplist as $ip) {
                    if (self::validateIp($ip)) {
                        return $ip;
                    }
                }
            } else {
                if (self::validateIp($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                    return $_SERVER['HTTP_X_FORWARDED_FOR'];
                }
            }
        }
        if (!empty($_SERVER['HTTP_X_FORWARDED'])
            && self::validateIp($_SERVER['HTTP_X_FORWARDED'])
        ) {
            return $_SERVER['HTTP_X_FORWARDED'];
        }
        if (!empty($_SERVER['HTTP_X_CLUSTER_CLIENT_IP'])
            && self::validateIp($_SERVER['HTTP_X_CLUSTER_CLIENT_IP'])
        ) {
            return $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
        }
        if (!empty($_SERVER['HTTP_FORWARDED_FOR'])
            && self::validateIp($_SERVER['HTTP_FORWARDED_FOR'])
        ) {
            return $_SERVER['HTTP_FORWARDED_FOR'];
        }
        if (!empty($_SERVER['HTTP_FORWARDED'])
            && self::validateIp($_SERVER['HTTP_FORWARDED'])
        ) {
            return $_SERVER['HTTP_FORWARDED'];
        }
    
        // Return unreliable IP address since all else failed
        return $_SERVER['REMOTE_ADDR'];
    }
    
    /**
     * Ensures an IP address is both a valid IP address and does not fall within
     * a private network range.
     * 
     * @param string $ip Request IP
     * 
     * @return boolean
     */
    public static function validateIp(string $ip)
    {
    
        if (strtolower($ip) === 'unknown') {
            return false;
        }
    
        // Generate IPv4 network address
        $ip = ip2long($ip);
    
        // If the IP address is set and not equivalent to 255.255.255.255
        if ($ip !== false && $ip !== -1) {
            // Make sure to get unsigned long representation of IP address
            // due to discrepancies between 32 and 64 bit OSes and
            // signed numbers (ints default to signed in PHP)
            $ip = sprintf('%u', $ip);
    
            // Do private network range checking
            if ($ip >= 0 && $ip <= 50331647) {
                return false;
            }
            if ($ip >= 167772160 && $ip <= 184549375) {
                return false;
            }
            if ($ip >= 2130706432 && $ip <= 2147483647) {
                return false;
            }
            if ($ip >= 2851995648 && $ip <= 2852061183) {
                return false;
            }
            if ($ip >= 2886729728 && $ip <= 2887778303) {
                return false;
            }
            if ($ip >= 3221225984 && $ip <= 3221226239) {
                return false;
            }
            if ($ip >= 3232235520 && $ip <= 3232301055) {
                return false;
            }
            if ($ip >= 4294967040) {
                return false;
            }
        }
        return true;
    }
}