<?php

use Dotenv\Dotenv;
use PHPUnit\Framework\TestCase;
use TPaga\Ledger;

class LedgerTest extends TestCase
{

    public function testBalance() {
        $dotenv = Dotenv::createImmutable(dirname(__DIR__));
        $dotenv->load();

        $this->assertSame(200, Ledger::balance()->getStatusCode());
    }

    /**
     * @dataProvider transferProvider
     */
    public function testTransfer(
        string $account,
        string $bucket,
        string $description,
        int $ammount,
        string $tx_id,
        int $expected
    ) {
        $dotenv = Dotenv::createImmutable(dirname(__DIR__));
        $dotenv->load();

        $this->assertSame($expected, Ledger::transfer(
            $account,
            $bucket,
            $description,
            $ammount,
            $tx_id
        )->getStatusCode());
    }

    public function transferProvider()
    {
        $dotenv = Dotenv::createImmutable(dirname(__DIR__));
        $dotenv->load();
        
        return [
            ['+573102795117', 'tpaga', 'prueba', 10000, 'TEST_' . (new DateTime('now'))->format('Ymd_His.u'), 201],
            ['+573102795117', 'tpaga', 'prueba', 1000, 'TEST_' . (new DateTime('now'))->format('Ymd_His.u'), 201],
            ['+573102795117', 'tpaga', 'prueba', 100000, 'TEST_' . (new DateTime('now'))->format('Ymd_His.u'), 201],
        ];
    }

}